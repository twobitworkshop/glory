# Glory Rules

## Components

1. Character cards
2. Trait tokens
3. Realm cards
4. Resource cards
5. King Token
6. Health tokens (wood blocks)

## About

You are the head of your household as the Count of a small province in a grand kingdom. You will compete with the other Lords and Ladies in order to rise in power and glory. It is not enough to become a King; in order for your family to be remembered for all time, you will have to also be a good King.

## Goal

In order to win, you must amass the most glory for your household. Time limit likely.

## Setup

Shuffle the Character Cards into a single deck and place it in the center of the table. This will be where future characters are drawn from when heirs take over.

Everyone draws three character cards and chooses one to be the starting lord or lady of their household.

Place the appropriate number of health tokens on the character's portrait.

The remaining health tokens are placed in a pool in the center of the table.

The King token is granted to the player at the table who can trace their lineage closest to a king. Otherwise assume everyone is related to Charlemagne and choose randomly.

Depending on distance from the King player, award starting bonuses to everyone, going clockwise, like so:

King 
    -> +1 Glory
    -> +2 Glory
    -> +3 Glory
    -> +3 Glory +1 Health
    -> +3 Glory +2 Health
    -> +3 Glory +3 Health

## Gameplay

Each round, a significant amount of time will pass as characters make decisions on how best to rule their lands. Characters will grow old and die, Kings will rise and fall, there will be wars, famine, deceits, and triumphs, and the most glorious household will reign supreme.

The game is composed of a series of rounds, which represent many years passing. Each round, all players will get the chance to have their current lord take a significant action and deal with threats to the realm. Threats will accumulate if not dealt with.

### A Round

A round is composed of three main phases.

``` Turn order: The order that all non-simultaneous actions are taken in this game is to always begin with the player who controls the King token and then clockwise from there. If there are ties to be dealt with, break ties in this manner. The distance a player is in turn order from the King is their house's relative political power. The last player is considered to be far removed from the politics of the crown.```

1. Threat Phase. Each player draws a Realm card and secretly determines its requirements and effects. A realm card will have its effect enacted during a specific phase, at which point it should be handled. If two realm card effects have overlapping timing, use the turn order to deal with them, one at a time. It is possible for turn order to change before all realm cards are dealt with.

2. Action Phase. Each player will select, in turn order, one action to take this round. The available actions are the same for all players, but a player's current lord or one of their lord's traits may provide them with additional options. Most actions will require spending some number of health tokens. If this is the case, they are removed from the character card and placed in the pool in the center of the table.

3. Liege Phase. Many realm cards will have an effect that takes place during the liege phase. Once all realm cards have been dealt with for the round, players finally have the option to gain glory by storing any resources in their hands into the vaults.

``` The Vault: The vault holds all glory for your household throughout the game. Simply stack resource cards face down in a pile to form your vault. You may check their values at any time. With some exception, once a card is placed in your vault, you will not be able to use it for actions or realm cards. At the end of the game, the cards in your vault will determine your glory ```

### Realm Cards

Realm cards represent the pressing issues that are facing your realm. In many cases, the effects will be shared as the threat or benefit spreads to other players.

#### Facing

If your realm card says "Face Up" then you must flip it face up for everyone to see as soon as it is drawn. It will still belong to the player who drew it. In all other cases, the card should remain hidden.

#### Timing

Each realm card will have a phase listed for when its effect takes place. This always occurs at the end of a phase, giving players a chance to react.

#### Task

The task section will detail requirements that must be met by the end of the phase in the timing section. Some Realm cards have no requirements, and in this case, the effect will always occur.

#### Effect

Simple effects occur at the appropriate timing. Task effects can have different outcomes depending on whether or not the task was completed. There will be a section for each outcome and only one is chosen, depending upon the result of the task.

#### Diagram

Card layout top to bottom

```
Name
Flavor
Facing
Task
Effect
Timing
```

### Actions

#### Basic Actions

All characters have access to all basic actions

#### Character Actions

#### Trait Actions

#### Realm Actions

### Contradictions

In some cases, certain realm cards or character cards / traits will contradict a rule in this book. In this case, realm cards always take priority, followed by traits, and finally characters.